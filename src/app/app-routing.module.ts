import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/**
 * agregando los componentes
 */
import { HomeComponent } from './components/home/home.component';
import { PeopleComponent } from './components/people/people.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'people/:id',
    component: PeopleComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
