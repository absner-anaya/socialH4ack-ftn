import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, filter } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';


@Injectable()
export class GetSearchPeopleService {

  constructor(private http: HttpClient) { }

  /**
   * searchPeople
   */
  public searchPeople(name: String): Observable<any> {

    if (name === '') {
      return of([]);
    }

    return this.http.get('https://swapi.co/api/people/?search=' + name);
  }

}
