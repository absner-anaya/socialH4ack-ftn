
export interface IPeople {
    name?: String;
    gender?: String;
    birth_year?: String;
    eye_color?: String;
    hair_color?: String;
    height?: String;
    mass?: String;
    skin_color?: String;
    url?: String;
    films?: Array<any>;
    homeworld?: Array<any>;
    species?: Array<any>;
    starships?: Array<any>;
    vehicles?: Array<any>;
    created?: Date;
    edited?: Date;
}

export class People {
    name?: String;
    gender?: String;
    birth_year?: String;
    eye_color?: String;
    hair_color?: String;
    height?: String;
    mass?: String;
    skin_color?: String;
    url?: String;
    films?: Array<any>;
    homeworld?: Array<any>;
    species?: Array<any>;
    starships?: Array<any>;
    vehicles?: Array<any>;
    created?: Date;
    edited?: Date;
}
