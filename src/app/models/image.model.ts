
export interface IImage {
    accentColor?: String;
    contentSize?: String;
    contentUrl?: String;
    height?: Number;
    width?: Number;
    encodingFormat?: String;
    hostPageDisplayUrl?: String;
    hostPageUrl?: String;
    imageInsightsToken?: String;
    insightsMetadata?: any;
    name?: String;
    thumbnail?: {
        width: Number,
        height: Number
    };
    webSearchUrl?: String;
    thumbnailUrl?: String;
    imageId?: String;
    datePublished?: Date;
}

export class Image {
    accentColor?: String;
    contentSize?: String;
    contentUrl?: String;
    height?: Number;
    width?: Number;
    encodingFormat?: String;
    hostPageDisplayUrl?: String;
    hostPageUrl?: String;
    imageInsightsToken?: String;
    insightsMetadata?: any;
    name?: String;
    thumbnail?: {
        width: Number,
        height: Number
    };
    webSearchUrl?: String;
    thumbnailUrl?: String;
    imageId?: String;
    datePublished?: Date;
}





