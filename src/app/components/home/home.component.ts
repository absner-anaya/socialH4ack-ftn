import { Component, ViewChild, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';

import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/merge';

/**
 * import servicios
 */
import { GetSearchPeopleService } from '../../services/get.searchPeople.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [
    GetSearchPeopleService
  ]
})
export class HomeComponent implements OnInit {

  /**
   * Declaración de variables
   */
  public myControl: FormControl = new FormControl();
  public filteredStates: Observable<any[]>;
  public respuesta: any;
  public people: any;
  public id_people: String;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private GetSearchPeople: GetSearchPeopleService
  ) { }

  ngOnInit() {
  }

  /**
   * searchPeople
   */
  public searchPeople(item: any) {
    console.log(item);
    this.GetSearchPeople.searchPeople(item).subscribe((response) => {
      this.respuesta  = response.results;
    }, (error) => {
      console.log(error);
    });

  }

  /**
   * selectPeople
   */
  public selectPeople(people: any) {
    this.people = people;
    const string_id_people: String = this.people.url.substr(-3);
    const id_people = string_id_people.split('/');

    id_people.forEach(item => {
      if (item === '') {
        console.log('Vacío');
      } else {
        this.id_people  = item;
      }
    });
    console.log(this.id_people);
    this.router.navigate(['people/', this.id_people]);

  }


}
