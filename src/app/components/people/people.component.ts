import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';

/**
 * imports models
 */
import { IPeople, People } from '../../models/people.model';
import { IImage, Image } from '../../models/image.model';

/**
 * imports servicos
 */
import { GetPeopleService } from '../../services/get.people.service';
import { NgbDateISOParserFormatter } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-parser-formatter';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss'],
  providers: [
    GetPeopleService
  ]
})
export class PeopleComponent implements OnInit {

  /**
   * Declaración de variables
   */
  public sub: any;
  public people: IPeople = new People();
  public imagePeople: Array<Image> = [];
  public filmsList: Array<any>  = [];
  public specieList: Array<any>  = [];
  public img1: IImage = new Image();
  public img2: IImage = new Image();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private GetPeople: GetPeopleService
  ) { }

  ngOnInit() {
    this.sub  = this.route.params.subscribe((_param)  =>  {
      this.getPeople(_param['id']);
    });
  }

  /**
   * getPeople
   */
  public getPeople(id_people: String) {
    this.GetPeople.getPeople(id_people).subscribe((response: IPeople) =>  {
      console.log(response);
      this.people = response;
      this.getImgPeople(response.name);
      this.getFilms(response.films);
      this.getSpecie(response.species);
    }, (error)  =>  {
      console.log(error);
    });
  }

  private getFilms(films: Array<any>) {
    films.forEach((item) => {
      this.GetPeople.getFilms(item).subscribe((response) => {
        // console.log(response);
        this.filmsList.push(response);
      }, (error) => {
        console.log(error);
      });
    });
    console.log(this.filmsList);
  }

  private getSpecie(specie: Array<any>) {
    specie.forEach((item) => {
      this.GetPeople.getFilms(item).subscribe((response) => {
        console.log(response);
        this.specieList.push(response);
      }, (error) => {
        console.log(error);
      });
    });
  }

  /**
   * getImgPeople
   */
  private getImgPeople(name: String) {
    this.GetPeople.getImage(name).subscribe((response) => {

      const arreglo: Array<IImage> = response.value;
      arreglo.forEach( (item: IImage, index) => {
        switch (index) {
          case 0:
            this.img1 = item;
            break;
          case 1:
            this.img2 = item;
            break;
          default:
            break;
        }
        // const image: IImage = new Image();
        // image.accentColor = item.accentColor;
        // image.contentSize = item.contentSize;
        // image.contentUrl  = item.contentUrl;
        // image.datePublished = item.datePublished;
        // image.encodingFormat  = item.encodingFormat;
        // image.height      = item.height;
        // image.hostPageDisplayUrl  = item.hostPageDisplayUrl;
        // image.hostPageUrl = item.hostPageUrl;
        // image.imageId   = item.imageId;
        // image.imageInsightsToken  = item.imageInsightsToken;
        // image.insightsMetadata  = item.insightsMetadata;
        // image.name  = item.name;
        // image.thumbnail = item.thumbnail;
        // image.thumbnailUrl  = item.thumbnailUrl;
        // image.webSearchUrl  = item.webSearchUrl;
        // image.width = item.width;
        // this.imagePeople.push(image);
      });
      // console.log(this.imagePeople);
    }, (error) => {
      console.log(error);
    });
  }

  /**
   * home
   */
  public home() {
    this.router.navigate(['/']);
  }

}
